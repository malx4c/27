//картинки
let images = ['assets/image/1.jpg','assets/image/2.jpg','assets/image/3.jpg','assets/image/4.jpg','assets/image/5.jpg'];
let num = 0;
//анимация
let moveLeft = false;
let moveUp = false;

// нажимаем кнопу "Вперед"
function forwImg() {
    let slider = document.getElementById('slider');
    num++;
    // если предыдущий был последнем в массиве
    if (num >= images.length) {
        num = 0;
    }
    slider.src = images[num];
}
// нажимаем кнопу "Назад"
function backImg() {
    let slider = document.getElementById('slider');
    num--;
    // если предыдущий был первым в массиве
    if (num < 0) {
        num = images.length - 1;
    }
    slider.src = images[num];
}

let idmove = 0;

function runAnimation() {
    // вызываем нашу функцию каждые 20 миллисекунд
    if (!idmove)  {
        document.getElementById('butStartStop').innerHTML='Стоп';
        idmove = setInterval(move, 20);
    } else {
        document.getElementById('butStartStop').innerHTML='Старт';
        clearInterval(idmove);
        idmove = 0;
    }
        
}

// получаем координаты элемента в контексте документа
function getCoords(elem) {
    let box = elem.getBoundingClientRect();

    return {
        top: box.top + window.pageYOffset,
        left: box.left + window.pageXOffset
    };
}

// будет перемещать каждые 20
// миллисекунд наш круг
function move() {
    
    
    // получаем доступ к кругу
    let circle = document.getElementById('circle')
    let container = document.getElementById('container');

    // вычисляем реальные координаты
    let cycleX = getCoords(circle).left;
    let cycleY = getCoords(circle).top;
    let containerX = getCoords(container).left;
    let containery = getCoords(container).top;

    // будем использовать переменные чтобы
    // записывать текущее положение круга
    let posX = cycleX - containerX;
    let posY = cycleY - containery;

    
    /* чтобы определить направление движения круга,
     проверяем достигли ли мы границы, сначала по горизонтали
    */
    if (posX >= 550) moveLeft = true; // значит дошли до правой границы
    if (posX <= 0) moveLeft = false; // значит дошли до левой границы
    if (posY >= 350) moveUp = true; // достигли нижней границы
    if (posY <= 0) moveUp = false; // достигли верхней границы

    // меняем позицию в зависимости
    // от направления движения
    moveLeft ?
        // по горизонтали
        posX = cycleX - 3 : posX = cycleX + 3;
    moveUp ?
        // по вертикали
        posY = cycleY - 1 : posY = cycleY + 1;
    // так мы двигаем наш круг

 
    circle.style.left = posX+'px';
    circle.style.top = posY+'px';
}

function getUniqueID() {
    return Date.now();
}